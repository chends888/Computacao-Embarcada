/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>


#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"



/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/
#define YEAR        2018
#define MONTH       3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0


#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   8
#define LED_PIN_MASK   (1<<LED_PIN)

#define LED1_PIO_ID ID_PIOA
#define LED1_PIO PIOA
#define LED1_PIN 0
#define LED1_PIN_MASK (1 << LED1_PIN)

#define LED2_PIO_ID ID_PIOC
#define LED2_PIO PIOC
#define LED2_PIN 30
#define LED2_PIN_MASK (1 << LED2_PIN)


#define BUT_PIO PIOD
#define BUT_PIO_ID ID_PIOD
#define BUT_PIN 28
#define BUT_PIN_MASK (1 << BUT_PIN)
#define BUT_DEBOUNCING_VALUE 79

/************************************************************************/
/* Global vars                                                          */
/************************************************************************/

// volatile uint8_t flag_alarm = 1;
// volatile uint8_t flag_led1 = 1;
uint32_t lcdminute;
// volatile int alarm_counter = 0;
uint32_t h, m, s;
int counter;

/************************************************************************/
/* Prototypes                                                          */
/************************************************************************/
void pin_toggle(Pio *pio, uint32_t mask);


/************************************************************************/
/* Handlers                                                             */
/************************************************************************/
static void BUT_Handler(uint32_t id, uint32_t mask){
	pio_clear(LED1_PIO, LED1_PIN_MASK);
	counter = 0;
	if (counter == 0){
		counter = 1;
		rtc_get_time(RTC, &h, &m, &s);
		rtc_set_time_alarm(RTC, 1, h, 1, m, 1, s+5);
	}
	else{
		counter += 1;
	}

}


void RTC_Handler(void){
	uint32_t ul_status = rtc_get_status(RTC);

	// interruption by time
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		rtc_get_time(RTC, &h, &m, &s);
		if (lcdminute != m){
			lcdminute = m;
			char buffer [10];
			sprintf(buffer, "%D:%D", h, m);
			gfx_mono_draw_string(buffer, 0, 0, &sysfont);
		}
	}
	// interruption by alarm (data or time)
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		// rtc_set_time_alarm(RTC, 1, h, 1, m+1, 1, s);
		if (counter == 1){
			for (int i = 0; i < 19; i++){
				pin_toggle(LED1_PIO, LED1_PIN_MASK);
				delay_ms(50);
			}
		}
		else{
			counter -= 1;
			rtc_get_time(RTC, &h, &m, &s);
			rtc_set_time_alarm(RTC, 1, h, 1, m, 1, s+2);
		}
		
		// pmc_disable_periph_clk(ID_TC1);
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}



/************************************************************************/
/* Inits                                                                */
/************************************************************************/

// Configure RTC by interruption
void RTC_init(){
	/* Configure o PMC */
	pmc_enable_periph_clk(ID_RTC);
	
	/* Default RTC configuration, 24-hour mode */
	// rtc_set_hour_mode(RTC, 0);
	
	// Configure date and time
	rtc_set_date(RTC, YEAR, MONTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);
	
	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);
	
	
	// Activate interruption by alarm and time (every sec)
	rtc_enable_interrupt(RTC, RTC_IER_ALREN | RTC_IER_SECEN);
	

}




void BUT_init(void){
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	
	pio_enable_interrupt(BUT_PIO, BUT_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIN_MASK, PIO_IT_FALL_EDGE, BUT_Handler);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
}


void LED_init(int state){
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, state, 0, 0 );
	
	// pmc_enable_periph_clk(LED2_PIO_ID);
	// pio_set_output(LED2_PIO, LED2_PIN_MASK, state, 0, 0 );
};



/************************************************************************/
/* Funcs                                                                */
/************************************************************************/
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio, mask);
}



int main (void)
{
	board_init();
	sysclk_init();
	
	delay_init();
	gfx_mono_ssd1306_init();
	
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
    // rtc_get_time(RTC, &h, &m, &s);
    // char buffer [10];
    // sprintf(buffer, "%D:%D", h, m);
	// gfx_mono_draw_string("", 0, 0, &sysfont);
	// gfx_mono_draw_string(buffer, 0, 0, &sysfont);	
	
	// Init RTC and LED
	RTC_init();
	LED_init(0);
	BUT_init();
	
	// Config RTC alarm
	// rtc_set_time_alarm(RTC, 1, MONTH, 1, MINUTE, 1, SECOND);
	// rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE, 1, SECOND+2);
	
	while(1) {
	
	}
}
