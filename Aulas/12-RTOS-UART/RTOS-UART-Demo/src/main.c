#include <asf.h>
#include <stdio.h>
#include <stdlib.h>
#include "conf_board.h"


/* Board */

/* Botao da placa */
#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

#define USART_COM_ID ID_USART1
#define USART_COM    USART1

/* RTOS  */
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (tskIDLE_PRIORITY)
#define TASK_UARTTX_STACK_SIZE             (2048/sizeof(portSTACK_TYPE))
#define TASK_UARTTX_STACK_PRIORITY         (tskIDLE_PRIORITY)
#define TASK_UARTRX_STACK_SIZE             (2048/sizeof(portSTACK_TYPE))
#define TASK_UARTRX_STACK_PRIORITY         (1)
#define TASK_PROCESS_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_PROCESS_STACK_PRIORITY        (2)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask, signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphoreBut;

/** Queue for msg log send data */
QueueHandle_t xQueueLedFreq;
QueueHandle_t xQueueChars;
uint32_t ucMsgLedFreq;

/** PROTOTYPES */
void but_callback(void);
static void BUT_init(void);
static void USART1_init(void);

/************************************************************************/
/* RTOS application funcs                                               */
/************************************************************************/

// brief Called if stack overflow during execution
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask, signed char *pcTaskName) {
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}


// brief This function is called by FreeRTOS idle task
extern void vApplicationIdleHook(void) {
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

// brief This function is called by FreeRTOS each tick
extern void vApplicationTickHook(void) {
}

extern void vApplicationMallocFailedHook(void) {
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

/************************************************************************/
/* handlers / callbacks                                                 */
/************************************************************************/

void but_callback(void) {
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	xSemaphoreGiveFromISR(xSemaphoreBut, &xHigherPriorityTaskWoken);
}


void USART1_Handler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	uint32_t ret = usart_get_status(USART_COM);
	char c;
	// Verifica por qual motivo entrou na interrup�cao
	if(ret & US_IER_RXRDY) { // Dado dispon�vel para leitura
		//printf("usart handler RX");
		usart_serial_getchar(USART1, &c);
		//usart_serial_putchar(USART1, &c);
		//printf("%c", c);
		xQueueSendFromISR(xQueueChars, &c, &xHigherPriorityTaskWoken);
	}
}


/************************************************************************/
/* TASKS                                                                */
/************************************************************************/

static void task_led(void *pvParameters) {
	/* cria queue com 32 "espacos" */
	/* cada espa�o possui o tamanho da variavel uCMsgLedFreq */
	xQueueLedFreq = xQueueCreate(32, sizeof(ucMsgLedFreq) );
	if (xQueueLedFreq == NULL)
		printf("falha em criar a queue \n");
  
	uint32_t msg = 0;
	uint32_t delayMs = 500;

	/* tarefas de um RTOS n�o devem retornar */
	for (;;) {
		/* verifica se chegou algum dado na queue, caso contrario */
		/* timeout = 0 */
		if( xQueueReceive(xQueueLedFreq, &msg, ( TickType_t ) 0 )) {
			/* chegou novo valor, atualiza delay ! */
			/* aqui eu poderia verificar se msg faz sentido (se esta no range certo) */
			/* converte ms -> ticks */
			delayMs = msg / portTICK_PERIOD_MS;
			printf("delay = %d \n", delayMs );
		}
		/* pisca LED */
		LED_Toggle(LED0);

		/* suspende por delayMs */
		vTaskDelay(delayMs);
	}
}

static void task_but(void *pvParameters) {
	/* Attempt to create a semaphore. */
	xSemaphoreBut = xSemaphoreCreateBinary();
	if (xSemaphoreBut == NULL)
		printf("falha em criar o semaforo \n");

	uint32_t delayTicks = 500 ;

	for (;;) {
		/* aguarda por tempo inderteminado at� a liberacao do semaforo */
		if( xSemaphoreTake(xSemaphoreBut, 0)) {
			/* atualiza frequencia */
			delayTicks -= 100;
			
			/* envia nova frequencia para a task_led */
			xQueueSend(xQueueLedFreq, (void *) &delayTicks, 10);

			/* garante range da freq. */
			if(delayTicks <= 100) {
				delayTicks = 500;
			}
		}
	}
}


static void task_chars(void *pvParameters) {
	// Como n�o foi especificado qual comando seria mandado atrav�s da UART,
	// Decidi alterar a frequ�ncia de piscada do LED.
	// Para isso configurei cada tecla com osendo um determinado delay em ms.
	// Pressione de 1 a 9 para alterar o delay, o bot�o ainda funciona normalmente.
	
	xQueueChars = xQueueCreate(32, sizeof(char));
	char uartChar;
	for (;;) {
		if ((xQueueReceive(xQueueChars, &uartChar, ( TickType_t ) 0)) == pdTRUE) {
			uint32_t newDelay;
			if (uartChar == '1') {
				newDelay = 50;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '2') {
				newDelay = 100;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '3') {
				newDelay = 150;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '4') {
				newDelay = 200;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '5') {
				newDelay = 250;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '6') {
				newDelay = 300;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '7') {
				newDelay = 350;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '8') {
				newDelay = 400;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
			if (uartChar == '9') {
				newDelay = 450;
				xQueueSend(xQueueLedFreq, (void *) &newDelay, 10);
			}
		}
	}
}




/************************************************************************/
/* FUNCOES                                                              */
/************************************************************************/

/**
 * \brief Configure the console UART.
 */
static void configure_console(void) {
	const usart_serial_options_t uart_serial_options = {.baudrate = CONF_UART_BAUDRATE,
		#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
		#endif
		.paritytype = CONF_UART_PARITY,
		#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
		#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
	#if defined(__GNUC__)
		setbuf(stdout, NULL);
	#else
		/* Already the case in IAR's Normal DLIB default configuration: printf()
		 * emits one character at a time.
		 */
	#endif
}





/************************************************************************/
/* INITS                                                                */
/************************************************************************/

static void BUT_init(void) {
	/* configura prioridae */
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 4);

	/* conf bot�o como entrada */
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT_PIO, BUT_PIO_PIN_MASK, 60);
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE , but_callback);

	//printf("Prioridade %d \n", NVIC_GetPriority(BUT_PIO_ID));
}

static void USART1_init(void) {
	/* Configura USART1 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4); // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;

	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate       = 115200,
		.char_length    = US_MR_CHRL_8_BIT,
		.parity_type    = US_MR_PAR_NO,
		.stop_bits   	= US_MR_NBSTOP_1_BIT,
		.channel_mode   = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART0 */
	sysclk_enable_peripheral_clock(USART_COM_ID);

	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART_COM, &usart_settings, sysclk_get_peripheral_hz());

	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART_COM);
	usart_enable_rx(USART_COM);

	/* map printf to usart */
	ptr_put = (int (*)(void volatile*,char))&usart_serial_putchar;
	ptr_get = (void (*)(void volatile*,char*))&usart_serial_getchar;

	/* ativando interrupcao */
	usart_enable_interrupt(USART_COM, US_IER_RXRDY);
	NVIC_SetPriority(USART_COM_ID, 4);
	NVIC_EnableIRQ(USART_COM_ID);
}

/************************************************************************/
/* main                                                                 */
/************************************************************************/

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void) {
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();
	USART1_init();

	/* iniciliza botao */
	BUT_init();

	// esse delay � necess�rio mas n�o entendo o porque !
	// sem ele o freertos considera que a interrupcao
	// do botao tem prioridade maior que a do systick
	// entrando em modo configASSERT
	// estudar :
	//  - https://dzone.com/articles/arm-cortex-m-interrupts-and-freertos-part-1
	//  - https://www.freertos.org/RTOS-Cortex-M3-M4.html
	delay_ms(100);

	/* Create task to make led blink */
	if (xTaskCreate(task_led, "Led", TASK_LED_STACK_SIZE, NULL, TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create led task\r\n");
	}

	/* Create task for button */
	if (xTaskCreate(task_but, "But", TASK_UARTTX_STACK_SIZE, NULL, TASK_UARTTX_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create but task\r\n");
	}
	
	
	/* Create task for create string */
	if (xTaskCreate(task_chars, "Chars", TASK_UARTTX_STACK_SIZE, NULL, TASK_UARTTX_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create chars task\r\n");
	}
	
	/* Start the scheduler. */
	vTaskStartScheduler();
	
	while(1){
	}

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
