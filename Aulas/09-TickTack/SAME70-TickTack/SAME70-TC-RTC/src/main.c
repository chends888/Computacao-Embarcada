#include "asf.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/
#define HOUR        15
#define MINUTE      58
#define SECOND      0


// LEDs

#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   8
#define LED_PIN_MASK   (1<<LED_PIN)


/************************************************************************/
/* Global vars                                                       */
/************************************************************************/

volatile uint8_t flag_led0 = 0;
uint32_t h, m, s;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void pin_toggle(Pio *pio, uint32_t mask);
void LED_init();
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init();

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

// Interrupt handler for TC0 interrupt.
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	// Devemos indicar ao TC que a interrup??o foi satisfeita
	
	ul_dummy = tc_get_status(TC0, 0);

	// Avoid compiler warning
	UNUSED(ul_dummy);

	// Change LED state
	if(flag_led0){
		pin_toggle(LED_PIO, LED_PIN_MASK);
	}
}



void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
			rtc_clear_status(RTC, RTC_SCCR_ALRCLR);

			flag_led0 = !flag_led0;
			
			rtc_get_time(RTC, &h, &m, &s);
			
			if(m == 59) {
				m = -1;
				h += 1;
			}
			
			pmc_disable_periph_clk(ID_TC1);
			
			rtc_set_time_alarm(RTC, 1, h, 1, m+1, 1, s);
			
			rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}


/************************************************************************/
/* Functions                                                              */
/************************************************************************/

// Toggle pin by PIO (out)
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

// @Brief Inicializa o pino do LED

void LED_init(){
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, 1, 0, 0 );
};

/*
Configura TimerCounter (TC) para gerar uma interrup??o no canal (ID_TC e TC_CHANNEL)
na taxa de especificada em freq.
*/

void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq) {
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	/* Configura o PMC */
	/* O TimerCounter meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	// Configura o TC para operar em  4Mhz e interrup??o no RC compare
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup??o no TC canal 0 */
	/* Interrup??o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	// Inicializa o canal 0 do TC
	tc_start(TC, TC_CHANNEL);
}
// Configura o RTC para funcionar com interrup??o
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);
	
	
	// Configure date and time
	
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);
	
	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);
	
	
	// Activate interruption by alarm
	rtc_enable_interrupt(RTC, RTC_IER_ALREN);
}

/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main (void) {
	// Initialize the SAM system
	sysclk_init();

	// Disable the Watchdog
	WDT->WDT_MR = WDT_MR_WDDIS;

	// Configura os LEDs
	LED_init();

	// Configura o RTC
	RTC_init();
	
	// Config RTC alarm
	rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE, 1, SECOND+10);
	
	// Configura timer TC0, canal 0
	TC_init(TC0, ID_TC0, 0, 10);


	while (1) {
		// Entrar em modo sleep
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
	}
}