/**
 * 5� semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configura��o de clock
 *  - Configur��o pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

#define BUT_PIO
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
#define BUT_PIO PIOA

/************************************************************************/
/* constantes                                                            */
/************************************************************************/

/************************************************************************/
/* vari�veis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrup��es                                                         */
/************************************************************************/

/************************************************************************/
/* fun��es                                                              */
/************************************************************************/

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){



	// Initialize the board clock
	sysclk_init();
	
	// Disable WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;



	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	// Initialize PCS as output
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while (1).
	while (1) {
		int but_status = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
		
		if (but_status == 0) {
			int i = 0;
			while (i < 5) {
				pio_set(LED_PIO, LED_PIO_PIN_MASK);
				delay_ms(200);
				
				pio_clear(LED_PIO, LED_PIO_PIN_MASK);
				delay_ms(200);
				i++;
			}	
		}
		
		else {
			pio_set(LED_PIO, LED_PIO_PIN_MASK);
		}
		
	}
	return 0;
}
