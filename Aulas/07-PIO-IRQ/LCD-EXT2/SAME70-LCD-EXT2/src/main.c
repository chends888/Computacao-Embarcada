#include "asf.h"
//#include <string.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include "ioport.h"
//#include "logo.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL


// LED
#define LED_PIO_ID ID_PIOC
#define LED_PIO_PIN 30
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

// Button 1
#define BUT_PIO_ID ID_PIOD
#define BUT_PIO_PIN 28
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
#define BUT_PIO PIOD


// Button 2
#define BUT2_PIO_ID ID_PIOC
#define BUT2_PIO_PIN 31
#define BUT2_PIO_PIN_MASK (1 << BUT2_PIO_PIN)
#define BUT2_PIO PIOC

// Button 3
#define BUT3_PIO_ID ID_PIOA
#define BUT3_PIO_PIN 19
#define BUT3_PIO_PIN_MASK (1 << BUT3_PIO_PIN)
#define BUT3_PIO PIOC


struct ili9488_opt_t g_ili9488_display_opt;



//brief Configure UART console.
 
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */

volatile but_flag_low = false;
volatile but_flag_high = false;
int delay = 100;
float freq;

void but_callBack_l (void) {
	but_flag_low = true;
}

void but_callBack_h (void) {
	but_flag_high = true;
}

void pisca_led(int delay){
	for(int counter = 0; counter < 5; counter++){
		pio_set(LED_PIO, LED_PIO_PIN_MASK);
		delay_ms(delay);
		pio_clear(LED_PIO, LED_PIO_PIN_MASK);
		delay_ms(delay);
	}
}



int main(void)
{
	// array para escrita no LCD
	uint8_t stringLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);
	
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
		
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_enable_interrupt(BUT2_PIO, BUT2_PIO_PIN_MASK);

	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, BUT_PIO_PIN,  but_callBack_l);
	
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIO_PIN_MASK, BUT2_PIO_PIN,  but_callBack_h);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);
	
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 0);

    /* Inicializa e configura o LCD */
	configure_lcd();


    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	

	while (1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);	
		// If button 1 is pressed, lower frequency
		if (but_flag_low) {
			delay += 40;
			freq = 1/(0.001*delay*2);
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
			ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
			sprintf(stringLCD, "Freq.: %f", freq);
			ili9488_draw_string(10, 300, stringLCD);
			but_flag_low = false;
		}
		
		// If button 2 is pressed, higher frequency
		else if (but_flag_high) {
			delay -= 40;
			if (delay < 40) {
				delay = 40;
				freq = 1/(0.001*delay*2);
				ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
				ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
				ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
				sprintf(stringLCD, "Freq.: %f", freq);
				ili9488_draw_string(10, 300, stringLCD);
			}
			freq = 1/(0.001*delay*2);
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
			ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
			sprintf(stringLCD, "Freq.: %f", freq);
			ili9488_draw_string(10, 300, stringLCD);
			but_flag_high = false;
		}
		
	pisca_led(delay);
	}
	return 0;
}