#include "asf.h"
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE        (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY    (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE            (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY        (tskIDLE_PRIORITY)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

// Configuring OLED board LED's and buttons (Connected to EXT1)
// LED's
#define LED1_PIO_ID		    ID_PIOA
#define LED1_PIO_PIN		0
#define LED1_PIO_PIN_MASK   (1 << LED1_PIO_PIN)
#define LED1_PIO            PIOA

#define LED2_PIO_ID		    ID_PIOC
#define LED2_PIO_PIN		30
#define LED2_PIO_PIN_MASK   (1 << LED2_PIO_PIN)
#define LED2_PIO            PIOC

#define LED3_PIO_ID		    ID_PIOB
#define LED3_PIO_PIN		2
#define LED3_PIO_PIN_MASK   (1 << LED3_PIO_PIN)
#define LED3_PIO            PIOB

// Buttons
#define BUT1_PIO_ID         ID_PIOD
#define BUT1_PIO_PIN        28
#define BUT1_PIO_PIN_MASK   (1 << BUT1_PIO_PIN)
#define BUT1_PIO            PIOD

#define BUT2_PIO_ID         ID_PIOC
#define BUT2_PIO_PIN        31
#define BUT2_PIO_PIN_MASK   (1 << BUT2_PIO_PIN)
#define BUT2_PIO            PIOC

#define BUT3_PIO_ID        ID_PIOA
#define BUT3_PIO_PIN       19
#define BUT3_PIO_PIN_MASK  (1 << BUT3_PIO_PIN)
#define BUT3_PIO           PIOA

// Prototypes
void but1_callback(void);
void but2_callback(void);
void but3_callback(void);
void init_but_board(void);



// Semaphores
SemaphoreHandle_t xSemaphore1;
SemaphoreHandle_t xSemaphore2;
SemaphoreHandle_t xSemaphore3;

/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask, signed char *pcTaskName) {
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.*/
	for (;;) {
	}
}

// brief This function is called by FreeRTOS idle task
extern void vApplicationIdleHook(void) {
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

// brief This function is called by FreeRTOS each tick
extern void vApplicationTickHook(void) {
}

extern void vApplicationMallocFailedHook(void) {
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

// Toggle pin by PIO (out)
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

// brief This task, when activated, make LED blink at a fixed rate
static void task_led1(void *pvParameters) {
	int ledflag = false;
	xSemaphore1 = xSemaphoreCreateBinary();
	for (;;) {
		
		if( xSemaphoreTake(xSemaphore1, ( TickType_t ) 10) == pdTRUE ) {
			// flag = true to enter the following 'if'
			ledflag = !ledflag;
			// Turn off LED
			pio_set(LED1_PIO, LED1_PIO_PIN_MASK);
		}
		if (ledflag) {
			pin_toggle(LED1_PIO, LED1_PIO_PIN_MASK);
		}
	}
}
static void task_led2(void *pvParameters) {
	int ledflag = false;
	xSemaphore2 = xSemaphoreCreateBinary();

	for (;;) {
		if( xSemaphoreTake(xSemaphore2, ( TickType_t ) 30) == pdTRUE ) {
			// flag = true to enter the following 'if'
			ledflag = !ledflag;
			// Turn off LED
			pio_set(LED2_PIO, LED2_PIO_PIN_MASK);
		}
		if (ledflag) {
			pin_toggle(LED2_PIO, LED2_PIO_PIN_MASK);
		}
	}
}
static void task_led3(void *pvParameters) {
	int ledflag = false;
	xSemaphore3 = xSemaphoreCreateBinary();

	for (;;) {
		if( xSemaphoreTake(xSemaphore3, ( TickType_t ) 60) == pdTRUE ) {
			// flag = true to enter the following 'if'
			ledflag = !ledflag;
			// Turn off LED
			pio_set(LED3_PIO, LED3_PIO_PIN_MASK);
		}
		if (ledflag) {
			pin_toggle(LED3_PIO, LED3_PIO_PIN_MASK);
		}
	}
}


// Button callbacks
void but1_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	xSemaphoreGiveFromISR(xSemaphore1, &xHigherPriorityTaskWoken);
}
void but2_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	xSemaphoreGiveFromISR(xSemaphore2, &xHigherPriorityTaskWoken);
}
void but3_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	xSemaphoreGiveFromISR(xSemaphore3, &xHigherPriorityTaskWoken);
}

void init_but_board(void){
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 8);
	pio_configure(BUT1_PIO, PIO_INPUT, BUT1_PIO_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT1_PIO, BUT1_PIO_PIN_MASK, 60);
	pio_enable_interrupt(BUT1_PIO, BUT1_PIO_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIO_PIN_MASK, PIO_IT_FALL_EDGE , but1_callback);
	
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 8);
	pio_configure(BUT2_PIO, PIO_INPUT, BUT2_PIO_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT2_PIO, BUT2_PIO_PIN_MASK, 60);
	pio_enable_interrupt(BUT2_PIO, BUT2_PIO_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIO_PIN_MASK, PIO_IT_FALL_EDGE , but2_callback);
	
	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 8);
	pio_configure(BUT3_PIO, PIO_INPUT, BUT3_PIO_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT3_PIO, BUT3_PIO_PIN_MASK, 60);
	pio_enable_interrupt(BUT3_PIO, BUT3_PIO_PIN_MASK);
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIO_PIN_MASK, PIO_IT_FALL_EDGE , but3_callback);
}

void LED_init() {	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIO_PIN_MASK, 1, 0, 0 );
	
	pmc_enable_periph_clk(LED2_PIO_ID);
	pio_set_output(LED2_PIO, LED2_PIO_PIN_MASK, 1, 0, 0 );
	
	pmc_enable_periph_clk(LED3_PIO_ID);
	pio_set_output(LED3_PIO, LED3_PIO_PIN_MASK, 1, 0, 0 );
};


int main(void) {
	/* Initialize the SAM system */
	sysclk_init();
	board_init();
	
	// Init LED's
	LED_init();
	
	// Init buttons
	init_but_board();

	/* Create tasks to make LED's blink */
	if (xTaskCreate(task_led1, "Led1", TASK_LED_STACK_SIZE, NULL, TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
	}
	if (xTaskCreate(task_led2, "Led2", TASK_LED_STACK_SIZE, NULL, TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
	}
	if (xTaskCreate(task_led3, "Led3", TASK_LED_STACK_SIZE, NULL, TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
	}

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
