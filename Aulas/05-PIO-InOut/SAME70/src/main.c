/**
 * 5� semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configura��o de clock
 *  - Configur��o pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)


/*  Default pin configuration (no attribute). */
#define _PIO_DEFAULT             (0u << 0)
/*  The internal pin pull-up is active. */
#define _PIO_PULLUP              (1u << 0)
/*  The internal glitch filter is active. */
#define _PIO_DEGLITCH            (1u << 1)
/*  The pin is open-drain. */
#define _PIO_OPENDRAIN           (1u << 2)
/*  The internal debouncing filter is active. */
#define _PIO_DEBOUNCE            (1u << 3)


/************************************************************************/
/* constantes                                                            */
/************************************************************************/


/************************************************************************/
/* vari�veis globais                                                    */
/************************************************************************/


/************************************************************************/
/* interrup��es                                                         */
/************************************************************************/


/************************************************************************/
/* fun��es                                                              */
/************************************************************************/

// fun��o que substitui a nativa pio_set
void _pio_set(Pio *p_pio, const uint32_t ul_mask) {
	p_pio->PIO_SODR = ul_mask;
}

// fun��o que substitui a nativa pio_clear
void _pio_clear(Pio *p_pio, const uint32_t ul_mask) {
	p_pio->PIO_CODR = ul_mask;
}


void _pio_pull_up(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_pull_up_enable) {
	if (ul_pull_up_enable){
		p_pio->PIO_PUER = ul_mask;
	}
	
	else {
		p_pio->PIO_PUDR = ul_mask;
	}

}

void _pio_set_output(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_default_level, const uint32_t ul_multidrive_enable, const uint32_t ul_pull_up_enable) {
	p_pio->PIO_OER = ul_mask;
	
	if (ul_default_level) {
		_pio_set(p_pio, ul_mask);
	}
	else {
		_pio_clear(p_pio, ul_mask);
	}

	if (ul_multidrive_enable) {
		p_pio->PIO_MDER = ul_mask;
	}
	else {
		p_pio->PIO_MDDR = ul_mask;
	}
	
	_pio_pull_up(p_pio, ul_mask, ul_pull_up_enable);

}

void _pio_set_input(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_attribute) {
	
}



/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Fun��o principal chamada na inicaliza��o do uC.
int main(void){



	// Initialize the board clock
	sysclk_init();
	
	// Disable WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;



	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	// Initialize PCS as output
	// pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	_pio_set_output(LED_PIO, LED_PIO_PIN_MASK, 0, 0, 1);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_DEFAULT);
	// _pio_set_output(p_pio, ul_mask, ul_default_level, ul_multidrive_enable, ul_pull_up_enable);
	_pio_pull_up(BUT_PIO, BUT_PIO_PIN_MASK, 1);
	
	
	// super loop
	// aplica��es embarcadas n�o devem sair do while (1).
	while (1) {
		int but_status = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
		
		if (but_status == 0) {
			int i = 0;
			while (i < 5) {
				_pio_set(LED_PIO, LED_PIO_PIN_MASK);
				delay_ms(200);
				
				_pio_clear(LED_PIO, LED_PIO_PIN_MASK);
				delay_ms(200);
				i++;
			}	
		}
		
		else {
			_pio_set(LED_PIO, LED_PIO_PIN_MASK);
		}
		
	}
	return 0;
}
